﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace DATAmining
{
    class Program
    {
        static void Main(string[] args)
        {

            List<User> friends = new List<User>();
            List<string> groups1 = new List<string>();
            groups1.Add("Аниме");
            groups1.Add("Спорт");
            groups1.Add("Танцы");
            groups1.Add("Музыка");
            groups1.Add("Mashups");
            List<string> groups2 = new List<string>();
            groups2.Add("Аниме");
            groups2.Add("Спорт");
            groups2.Add("Mashups");
            List<string> groups3 = new List<string>();
            groups3.Add("Аниме");
            groups3.Add("Спорт");
            List<string> groups4 = new List<string>();
            groups4.Add("Спорт");
            groups4.Add("Танцы");
            groups4.Add("Музыка");
            List<string> groups5 = new List<string>();
            groups5.Add("Mashups");
            List<string> groups6 = new List<string>();
            groups6.Add("Mashups");
            groups6.Add("Спорт");
            groups6.Add("Танцы");

            friends.Add(new User { Name = "Vlad", Friends = null, Groups = groups1 });
            friends.Add(new User { Name = "Vadim", Friends = null, Groups = groups2 });
            friends.Add(new User { Name = "Vladlena", Friends = null, Groups = groups3 });
            friends.Add(new User { Name = "Vitaliy", Friends = null, Groups = groups4 });
            friends.Add(new User { Name = "Victim", Friends = null, Groups = groups5 });

            User user = new User {Name="Igor", Friends=friends, Groups= groups6 };

            Console.WriteLine("Введите точность совпадений групп (1-3):");
            int count = int.Parse(Console.ReadLine());
            var f = user.Friends
                .Select(t => new { t.Groups, friendsNames = t.Name })
                .Select(t => new {count = t.Groups.Intersect(user.Groups).Count(), name = t.friendsNames}).Where(t=> t.count >= count);
            foreach(var c in f)
            {
                Console.WriteLine(c.name);                
            }

            Console.WriteLine();
            //Задание 2
            int srsum = 0;
            int ccount = 0;
            var prod_count = Prod_Basket.DataProd_Basket()
                .GroupBy(r => r.PROD_CODE).Select(r => new { prod = r.Key, count = r.Select(f => f.BASKET_ID).Count() })
                .OrderByDescending(r=>r.count);

            foreach (var c in prod_count) 
            {
                srsum += c.count;
                ccount++;
            }
            srsum /= ccount;
            List<int> integer = new List<int>();
            foreach (var c in prod_count)
            {
                if (c.count > srsum)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    for(int i = 0; i<srsum; i++)
                        Console.Write("-");
                    Console.ResetColor();
                    Console.WriteLine(c.prod+" "+c.count+":");
                    for (int i = 0; i < c.count; i+=7)
                        Console.Write("*");
                    Console.WriteLine();
                   
                }
                integer.Add(c.count);   
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Hello, World!");
            Console.WriteLine("Задание №3");
            Console.WriteLine("Нажмите любую клавишу, чтобы распределить товары по средней цене");
            Console.ReadKey();
            Console.ResetColor();
            int ost = 0;
            foreach (var c in prod_count)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                for (int i = 0; i < srsum; i++)
                    Console.Write("-");
                Console.ResetColor();
                Console.WriteLine(c.prod + " " + c.count + ":");
                for (int i = 0; i < srsum; i++)
                {
                    Console.Write("*");
                }
                ost = c.count + ost - srsum;
                Console.WriteLine();
            }
            int j = 0;
            while (ost > 0)
            {
                integer.Add(srsum);
                ost -= srsum;
                Console.ForegroundColor = ConsoleColor.Red;
                for (int i = 0; i < srsum; i++)
                    Console.Write("-");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"New Prod! {j+1} " + srsum + ":");
                Console.ResetColor();
                if (ost <= 8)
                {
                    for (int i = 0; i<ost; i++)
                    {
                        Console.Write("*");
                    }
                    ost -= 8;
                    continue;
                }
                for (int i = 0; i < srsum; i++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
                j++;
            }

            
        }
    }
}
