﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;
namespace DATAmining
{
    class User 
    {
        public string Name { get; set; }       
        public List<User> Friends { get; set; }
        public List<string> Groups { get; set; }
            
    }
    class Prod_Basket
    {
        public string PROD_CODE { get; set; } 
        public string BASKET_ID { get; set; }

        public static List<Prod_Basket> DataProd_Basket()
        { 
            StreamReader sr = new StreamReader("transactions.csv");
            List<Prod_Basket> pr = new List<Prod_Basket>();
            string line;
            int count = 0;
            while ((line = sr.ReadLine())!=null)
            {
                count++;               
            }
            sr.BaseStream.Position = 0;
            while ((line = sr.ReadLine()) != null)
            {
                string[] lines = line.Split(';');
                pr.Add(new Prod_Basket { PROD_CODE = lines[0], BASKET_ID = lines[1] });
            }
            return pr;

        }
    }

}
